/**
 * 包含n个接口请求函数的模块
 * 函数的返回值：promise对象
 */
import ajax from './ajax'
// const BASE_URL = 'http://localhost:4000'
const BASE_URL = '/api'

// 1.根据经纬度获取位置详情
export const reqAddress = (geohash) => ajax(`${BASE_URL}/position/${geohash}`)
// 2.获取食品分类列表
// export const reqFoodTypes = () => ajax(BASE_URL + `/index_category`)
// 3.根据经纬度获取商铺列表
// export const reqShops = (longitude, latitude) => ajax(BASE_URL + `/shops`, { longitude, latitude })

/**
* 2.获取msite 页面食品分类列表
*/
export const reqCategorys = () => ajax('/api/index_category')
/**
* 3.获取msite 商铺列表(根据经纬度)
*/
export const reqShops = ({ latitude, longitude }) => ajax('/api/shops', {
  latitude,
  longitude
})
/**
* 4.根据经纬度和关键字搜索商铺列表
*/
export const reqSearchShops = ({ geohash, keyword }) => ajax('/api/search_shops', {
  geohash,
  keyword
})
/**
* 6.账号密码登录
*/
export const reqPwdLogin = (name, pwd, captcha) => ajax('/api/login_pwd', {
  name,
  pwd,
  captcha
}, 'POST')
/**
* 7.获取短信验证码
*/
export const reqSendCode = phone => ajax('/api/sendcode', { phone })
/**
* 8.手机号验证码登录
*/
export const reqSmsLogin = (phone, code) => ajax('/api/login_sms', { phone, code }, 'POST')
/**
* 9.获取用户信息(根据会话)
*/
export const reqUserInfo = () => ajax('/api/userinfo')

/*
10.请求登出
*/
export const reqLogout = () => ajax('/api/logout')
